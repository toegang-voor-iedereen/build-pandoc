#!/bin/bash

set -eu
if [ ! -d "pandoc" ]; then
  git clone https://github.com/jgm/pandoc.git
fi

docker run \
    --env-file .env \
    -v $(pwd)/pandoc:/mnt \
    -v $(pwd)/linux-amd64:/mnt/linux-amd64 \
    -v $(pwd)/build-in-container.sh:/build-in-container.sh \
    glcr.b-data.ch/ghc/ghc-musl:9.4.5 \
    bash /build-in-container.sh
#!/bin/bash

echo ""
echo "RUNNING SCRIPT build-in-container.sh"
echo ""

echo "\$ env"
env
echo ""

echo "\$ pwd"
pwd
echo ""

echo "\$ cd /mnt"
cd /mnt
echo ""

echo "\$ cabal update"
cabal update
echo ""

echo "\$ cabal build --dependencies-only $CABALOPTS --ghc-options="$GHCOPTS" pandoc-cli"
cabal build --dependencies-only $CABALOPTS --ghc-options="$GHCOPTS" pandoc-cli
echo ""

echo "\$ bash linux/make_artifacts.sh"
bash linux/make_artifacts.sh
echo ""

echo "\$ find linux-amd64/"
find linux-amd64/
echo ""

echo "\$ ls ."
ls .
echo ""